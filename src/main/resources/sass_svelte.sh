#!/bin/bash

set -e

echo "GTG# compiling svelte"
cd /home/DEV/Workspaces/intellij/lofidrox
npm run build

echo "GTG# Copying resources"
rm /home/DEV/Workspaces/intellij/lofidrox/src/WebContent/project/final/images/*
cp /home/DEV/Workspaces/intellij/lofidrox/src/WebContent/project/img/* /home/DEV/Workspaces/intellij/lofidrox/src/WebContent/project/final/images/
