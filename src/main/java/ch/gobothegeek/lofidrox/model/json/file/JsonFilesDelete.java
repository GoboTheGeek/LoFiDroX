package ch.gobothegeek.lofidrox.model.json.file;

// a json wrapper used to get file ids to delete
public class JsonFilesDelete {
	private Integer[] files = null;

	public JsonFilesDelete() { }

	public JsonFilesDelete(Integer[]files) {
		this.files = files;
	}

	public Integer[] getFiles() { return files; }
	public void setFiles(Integer[] files) { this.files = files; }
}
