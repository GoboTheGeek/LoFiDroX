package ch.gobothegeek.lofidrox.security.permissions;

public enum LfdPermissionType {
	// type of permission used on objects
	CREATE, READ, UPDATE, DELETE, EXECUTE
}
