package ch.gobothegeek.lofidrox.exceptions;

// base exception for this application
public class LfdException extends Exception {
	public LfdException(String message) {
		super(message);
	}
}
