import LoFiDroX from '../html/cmpLoFiDroX.svelte';

const app = new LoFiDroX( {
	target: document.body
} );

export default app;