let lfdUrlBase = '/lofidrox/';
let LfdUrls = {
    spa_error: lfdUrlBase + 'spa/error',
    spa_home: lfdUrlBase + 'spa/home',
    spa_user_login: lfdUrlBase + 'spa/login',
    spa_user_register: lfdUrlBase + 'spa/register',
    spa_user_logout: lfdUrlBase + 'spa/logout',
    spa_file_upload: lfdUrlBase + 'spa/upload',
    spa_file_inbox: lfdUrlBase + 'spa/inbox',

    crud_user_check: lfdUrlBase + 'crud/user/check',
    crud_user_login: lfdUrlBase + 'crud/user/login',
    crud_user_register: lfdUrlBase + 'crud/user/register',
    crud_user_logout: lfdUrlBase + 'crud/user/logout',
    crud_user_list: lfdUrlBase + 'crud/user/list',

    crud_file_send: lfdUrlBase + 'crud/file/send',
    crud_file_list: lfdUrlBase + 'crud/file/list',
    crud_file_down: lfdUrlBase + 'crud/file/download',
    crud_file_del: lfdUrlBase + 'crud/file/delete'
};

export default LfdUrls;